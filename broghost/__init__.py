# BroGhost, keep an eye on your servers like a brother
# Copyright © 2019 Guillaume Ayoub
import os
from datetime import datetime, timedelta, timezone

from flask import Flask, render_template, request

from influxdb import InfluxDBClient

SCRIPT_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
TEMPLATES_FOLDER = os.path.join(SCRIPT_DIRECTORY, "templates")

app = Flask(__name__, template_folder=TEMPLATES_FOLDER)

client = InfluxDBClient(
    host=os.getenv("INFLUXDB_HOST", 'localhost'),
    port=int(os.getenv("INFLUXDB_PORT", 8086)),
    username=os.getenv("INFLUXDB_USER", 'root'),
    password=os.getenv("INFLUXDB_PASSWORD", 'root'),
    database=os.getenv("INFLUXDB_DB", 'watchghost'),
)


def set_status(step_statuses, aggregate_statuses):
    for status in ('unknown', 'critical', 'error', 'warning', 'info'):
        if step_statuses.get(status):
            break
    else:
        status = 'unknown'
    step_statuses['status'] = status
    if status == 'info':
        aggregate_statuses['info'] += 1
    if status != 'unknown':
        aggregate_statuses['total'] += 1


@app.route('/')
def index():
    original_tag = tag = request.args.get('tag', 'server')
    original_delta = delta = request.args.get('delta', '24h')
    original_step = step = request.args.get('step')
    original_start = start = request.args.get('start')
    original_stop = stop = request.args.get('stop')

    tag = f'tag.{tag}'

    if start and stop:
        start = datetime.strptime(start.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        start = start.replace(tzinfo=timezone.utc)
        stop = datetime.strptime(stop.split('.')[0], '%Y-%m-%dT%H:%M:%S')
        stop = stop.replace(tzinfo=timezone.utc)
        delta = stop - start
        seconds = delta.total_seconds()
    else:
        seconds = 60 * 60
        if delta.endswith('d'):
            seconds *= 24
        seconds *= int(delta[:-1])
        delta = timedelta(seconds=seconds)
        stop = datetime.now(timezone.utc)
        start = stop - delta
    if seconds > 60 * 24 * 60 * 60:  # 60 days
        step = timedelta(days=1)
    elif seconds > 15 * 24 * 60 * 60:  # 15 days
        step = timedelta(hours=8)
    elif seconds > 4 * 24 * 60 * 60:  # 4 days
        step = timedelta(hours=2)
    elif seconds > 12 * 60 * 60:  # 12 hours
        step = timedelta(minutes=15)
    else:
        step = timedelta(minutes=5)

    if original_step:
        step = timedelta(seconds=int(original_step))

    query = client.query(
        'select count("status") '
        'from /.*/ '
        'where is_hard = true '
        'and time >= \'{}\' '
        'and time <= \'{}\' '
        'and "{}" != \'\' '
        'group by "tag.status", "{}", "tag.name", time({}s)'.format(
            start.isoformat(), stop.isoformat(), tag, tag,
            int(step.total_seconds())
        )
    )

    data = {}
    group_data = {}
    statuses = {}
    group_statuses = {}
    for (name, tags), values in query.items():
        if tags[tag] not in data:
            data[tags[tag]] = {}
            statuses[tags[tag]] = {'info': 0, 'total': 0}
            group_data[tags[tag]] = {}
            group_statuses[tags[tag]] = {}
        if tags['tag.name'] not in group_data[tags[tag]]:
            group_data[tags[tag]][tags['tag.name']] = {}
            group_statuses[tags[tag]][tags['tag.name']] = {
                'info': 0, 'total': 0}

        data_tag = data[tags[tag]]
        group_data_tag = group_data[tags[tag]][tags['tag.name']]

        for value in values:
            time = datetime.strptime(
                value['time'][:-1].split('.')[0], '%Y-%m-%dT%H:%M:%S')
            date = time.date().strftime('%B %d')
            if step.seconds:
                start = time.time().strftime('%H:%M')
                stop = (time + step).time().strftime('%H:%M')
                time = '{}\n{} → {} (UTC)'.format(date, start, stop)
            else:
                time = '{}'.format(date)

            if time not in data_tag:
                data_tag[time] = {tags['tag.status']: value['count']}
            elif tags['tag.status'] in data_tag[time]:
                data_tag[time][tags['tag.status']] += value['count']
            else:
                data_tag[time][tags['tag.status']] = value['count']

            if time not in group_data_tag:
                group_data_tag[time] = {tags['tag.status']: value['count']}
            elif tags['tag.status'] in group_data_tag[time]:
                group_data_tag[time][tags['tag.status']] += value['count']
            else:
                group_data_tag[time][tags['tag.status']] = value['count']

    for group, data_tag in data.items():
        for values in data_tag.values():
            set_status(values, statuses[group])
        for name, group_data_tag in group_data[group].items():
            for values in group_data_tag.values():
                set_status(values, group_statuses[group][name])

    uptimes = {
        key: ((100 * value['info'] / value['total'])
              if value['total'] else None)
        for key, value in statuses.items()
    }
    group_uptimes = {
        group_key: {
            key: ((100 * value['info'] / value['total'])
                  if value['total'] else None)
            for key, value in statuses.items()
        } for group_key, statuses in group_statuses.items()
    }

    query = client.query('show tag keys')
    tags_list = {}
    for item, tags in query.items():
        for tag in tags:
            if tag['tagKey'].startswith('tag.'):
                tag_name = tag['tagKey'][4:]
                if tag_name not in ('name', 'status'):
                    tags_list[tag_name.title()] = tag_name

    tag_names = {value: key for key, value in tags_list.items()}
    return render_template(
        'index.html.jinja2',
        title='Availability - {}'.format(tag_names[original_tag]),
        data=data, group_data=group_data, tags=tags_list,
        original_tag=original_tag, original_step=original_step,
        original_start=original_start, original_stop=original_stop,
        original_delta=original_delta, uptimes=uptimes,
        group_uptimes=group_uptimes,
    )
